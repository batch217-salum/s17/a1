/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function userProfile(){
	let userFullName = prompt("What is your name?");
	let userAge = prompt("How old are you?");
	let userLocation = prompt("Where do you live?");
	alert("Thank you for the input!");

	function userStorage(){
		let storedName = "Loyd"
		let storedAge = 30;
		let storedLocation = "New York City";

		console.log("Hello, "+ userFullName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userLocation);

	}

	userStorage();

}
userProfile();

function faveMusicBands(){
	console.log("1. Eraserheands");
	console.log("2. Rivermaya");
	console.log("3. Parokya ni Edgar");
	console.log("4. Sponge Cola");
	console.log("5. Silent Sanctuary");
}

faveMusicBands();


function faveMovies(){
	console.log("1. John Wick");
	console.log("Rotten Tomatoes Rating: 86%");

	console.log("2. White House Down");
	console.log("Rotten Tomatoes Rating: 51%");

	console.log("3. Saving Private Ryan");
	console.log("Rotten Tomatoes Rating: 94%");

	console.log("4. Ninja Assassin");
	console.log("Rotten Tomatoes Rating: 26%");

	console.log("5. 2012");
	console.log("Rotten Tomatoes Rating: 39%");

}

faveMovies();


// 4. Debugging Practice
// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);